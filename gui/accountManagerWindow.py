#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 13:22:43 2022

@author: alexpiration
"""

from PyQt5.QtWidgets import (
    QApplication,
    QMainWindow,
    QLabel,
    QPushButton,
    QLineEdit,
    QTableWidget,
    QTableWidgetItem,
    QStackedWidget,
    QWidget,
    QInputDialog,
    QMessageBox,
    QComboBox,
)
from PyQt5 import uic
import numpy as np
import pandas as pd
import sys
import os

from bank.user import User
from bank.bank_entrie import BankEntries
from gui.customInputDialog import EditInputDialog, DeleteInputDialog


class accountManagerWindow(QMainWindow):
    
    def __init__(self, parent_api):
        super(accountManagerWindow, self).__init__()
        
        self.parent_api = parent_api
        
        # Load ui file of login Window
        uic.loadUi(os.path.join("gui", "ui", "accountManagerWindow_VResponsive.ui"), self)
        
        # Get and Define Widgets presents in ui file
        ## Widget of main content
        self.account_name_label = self.findChild(QLabel, "account_name_label")
        self.sold_label = self.findChild(QLabel, "user_amount_label")
        self.stacked_widget = self.findChild(QStackedWidget, "main_content")
        self.stacked_widget.setCurrentWidget(self.findChild(QWidget, "account"))
        self.logout_btn = self.findChild(QPushButton, "logout_btn")
        self.logout_btn.clicked.connect(self.logout)
        
        self.category_filter_cbbox = self.findChild(QComboBox, "category_choice_combobox")
        category_dataset = self.parent_api.login_user.get_all_category()
        self.category_filter_cbbox.addItems(category_dataset)
        
        self.month_filter_cbbox = self.findChild(QComboBox, "month_choice_combobox")
        month_dataset = self.parent_api.login_user.get_all_bank_ent()
        month_year_list = list(dict.fromkeys([str(pd.to_datetime(element["date"]).month_name()) + " - " + str(pd.to_datetime(element["date"]).year) for element in month_dataset]))
        self.month_filter_cbbox.addItems(month_year_list)
        
        
        ## Widget of left menu
        self.main_btn = self.findChild(QPushButton, "main_btn")
        self.main_btn.clicked.connect(self.account_view)
        
        ### add menu
        self.add_spent_btn = self.findChild(QPushButton, "add_spent_btn")
        self.add_spent_btn.clicked.connect(self.add_spent_form)
        self.category_add_spent_form = self.findChild(QComboBox, "category_input_s")
        self.category_add_spent_form.addItems(self.parent_api.login_user.get_all_category())
        self.submit_new_spent_btn = self.findChild(QPushButton, "submit_btn_s")
        self.submit_new_spent_btn.clicked.connect(self.submit_new_spent)
        
        self.add_income_btn = self.findChild(QPushButton, "add_income_btn")
        self.add_income_btn.clicked.connect(self.add_income_form)
        self.category_add_income_form = self.findChild(QComboBox, "category_input_i")
        self.category_add_income_form.addItems(self.parent_api.login_user.get_all_category())
        self.submit_new_income_btn = self.findChild(QPushButton, "submit_btn_i")
        self.submit_new_income_btn.clicked.connect(self.submit_new_income)
        
        self.add_category_btn = self.findChild(QPushButton, "add_category_btn")
        self.add_category_btn.clicked.connect(self.add_category_form)
        
        
        ### filter menu
        self.filter_spent_btn = self.findChild(QPushButton, "filter_spent_btn")
        self.filter_spent_btn.clicked.connect(self.filter_spent_view)
        
        self.filter_income_btn = self.findChild(QPushButton, "filter_income_btn")
        self.filter_income_btn.clicked.connect(self.filter_income_view)
        
        self.filter_category_btn = self.findChild(QPushButton, "filter_category_btn")        
        self.filter_category_btn.clicked.connect(self.filter_category_view)
        
        self.filter_month_btn = self.findChild(QPushButton, "filter_month_btn")
        self.filter_month_btn.clicked.connect(self.filter_month_view)
        
        ## Configure size of width column of table which display account infos
        self.account_content_table = self.findChild(QTableWidget, "account_table_view")
        self.account_content_table.setColumnWidth(0, 218)
        self.account_content_table.setColumnWidth(1, 163.5)
        self.account_content_table.setColumnWidth(2, 163.5)
        self.account_content_table.setColumnWidth(3, 163.5)
        self.account_content_table.setColumnWidth(4, 163.5)
        
        # Edit and Delete btn
        self.delete_btn = self.findChild(QPushButton, "delete_entrie_btn")
        self.delete_btn.clicked.connect(self.delete_form)
        
        self.edit_btn = self.findChild(QPushButton, "edit_entrie_btn")
        self.edit_btn.clicked.connect(self.edit_form)
        
        
    def logout(self):
        self.parent_api.username_form.clear()
        self.parent_api.password_form.clear()
        self.parent_api.show()
        self.hide()
        
        
    def display_user_sold(self):
        sold_txt = self.sold_label.text()
        sold_value = self.parent_api.login_user.get_sold()
        
        if sold_value > 0:
            sold_value = "+" + str(sold_value)
            self.sold_label.setStyleSheet("color: #23433E")
            self.sold_label.setText(sold_txt.format(sold_value))
        else:
            sold_value = "-" + str(sold_value)
            self.sold_label.setStyleSheet("color: #432328")
            self.sold_label.setText(sold_txt.format(sold_value))
            
            
    def load_data_table(self):
        dataset = self.parent_api.login_user.get_all_bank_ent()
        row = 0
        self.account_content_table.setRowCount(len(dataset))
        
        for data in dataset:
            self.account_content_table.setItem(row, 0, QTableWidgetItem(data['title']))
            self.account_content_table.setItem(row, 1, QTableWidgetItem(data['type']))
            self.account_content_table.setItem(row, 2, QTableWidgetItem(str(np.round(data['amount'],2))))
            self.account_content_table.setItem(row, 3, QTableWidgetItem(str(data['date'])))
            self.account_content_table.setItem(row, 4, QTableWidgetItem(data['category']))
            row += 1
    
    
    def account_view(self):
        self.account_page = self.stacked_widget.widget(0)
        self.stacked_widget.setCurrentWidget(self.account_page)
        self.load_data_table()
        self.sold_label.setText(
            "+" + str(self.parent_api.login_user.get_sold()) if self.parent_api.login_user.get_sold() > 0
            else "-" + str(self.parent_api.login_user.get_sold())
        )
        
        
        
    # add function
    def add_spent_form(self):
        self.add_spent_form = self.stacked_widget.widget(1)
        self.stacked_widget.setCurrentWidget(self.add_spent_form)
        
        
    def submit_new_spent(self):        
        # Get data in each fields
        title_input = self.findChild(QLineEdit, "title_input_s").text().strip()
        amount_input = self.findChild(QLineEdit, "amount_input_s").text().strip()
        date_input = self.findChild(QLineEdit, "date_input_s").text().strip()
        category_input = self.category_add_spent_form.currentText()
        description_input = self.findChild(QLineEdit, "description_input_s").text().strip()
        
        if not title_input or not amount_input or not date_input:
            error_label = self.findChild(QLabel, "error_label_s")
            error_label.setText("Erreur, veuillez renseigner tous les champs obligatoires.")
            error_label.setStyleSheet("color: red;")
       
        else:
            new_spent = BankEntries(title_input, "Dépense", amount_input, date_input, self.parent_api.login_user.u_id, category=category_input, description=description_input)
            check = new_spent.add_entrie()
            
            msg = QMessageBox()
            if check:
                msg.setWindowTitle("Dépense bien ajoutée")
                msg.setText("La nouvelle dépense a bien été ajouté.")
                msg.exec_()
            else:
                msg.setWindowTitle("Erreur lors de l'ajout")
                msg.setText("Une erreur est survenue lors de l'ajout de la nouvelle dépense, veuillez réessager.")
                msg.exec_()
            
            
    def add_income_form(self):
        self.add_income_form = self.stacked_widget.widget(2)
        self.stacked_widget.setCurrentWidget(self.add_income_form)
        
    def submit_new_income(self):        
        # Get data in each fields
        title_input = self.findChild(QLineEdit, "title_input_i").text().strip()
        amount_input = self.findChild(QLineEdit, "amount_input_i").text().strip()
        date_input = self.findChild(QLineEdit, "date_input_i").text().strip()
        category_input = self.category_add_income_form.currentText()
        description_input = self.findChild(QLineEdit, "description_input_i").text().strip()
        
        if not title_input or not amount_input or not date_input:
            error_label = self.findChild(QLabel, "error_label_i")
            error_label.setText("Erreur, veuillez renseigner tous les champs obligatoires.")
            error_label.setStyleSheet("color: red;")
       
        else:
            new_income = BankEntries(title_input, "Rentrée", amount_input, date_input, self.parent_api.login_user.u_id, category=category_input, description=description_input)
            check = new_income.add_entrie()
            
            msg = QMessageBox()
            if check:
                msg.setWindowTitle("Rentrée bien ajoutée")
                msg.setText("La nouvelle rentrée d'argent a bien été ajouté.")
                msg.exec_()
            else:
                msg.setWindowTitle("Erreur lors de l'ajout")
                msg.setText("Une erreur est survenue lors de l'ajout de la nouvelle entrée d'argent, veuillez réessager.")   
                msg.exec_()      
    
    def add_category_form(self):
        category_input, ok_pressed = QInputDialog.getText(self, "Entrez la nouvelle catégorie à ajouter", "Catégorie : ", QLineEdit.Normal, "")
        if ok_pressed and category_input != '':
            check = User.add_category(category_input, self.parent_api.login_user.u_id)
            
            msg = QMessageBox()
            if check:
                msg.setWindowTitle("Categorie ajoutée")
                msg.setText(f"La catégorie {category_input} a bien été ajoutée")
                msg.exec_()
                
                self.category_add_spent_form.clear()
                self.category_add_spent_form.addItems(self.parent_api.login_user.get_all_category())
                
                self.category_add_income_form.clear()
                self.category_add_income_form.addItems(self.parent_api.login_user.get_all_category())
                
                self.category_filter_cbbox.clear()
                self.category_add_income_form.addItems(self.parent_api.login_user.get_all_category())
                
            else:
                msg.setWindowTitle("Categorie déjà existante")
                msg.setText(f"La catégorie {category_input} est déjà existante dans vos catégories")
                msg.exec_()
    
    # filter function
    def filter_spent_view(self):
        self.filter_spent_page = self.stacked_widget.widget(3)
        self.stacked_widget.setCurrentWidget(self.filter_spent_page)
        self.filter_spent_table = self.findChild(QTableWidget, "filter_spent_table")
        
        dataset = self.parent_api.login_user.get_all_bank_ent()
        type_filter = []
        for element in dataset:
            if element['type'] == "Dépense":
                type_filter.append(True)
            else:
                type_filter.append(False)
        
        filter_spent_data = dataset[type_filter]
        row = 0
        self.filter_spent_table.setRowCount(len(filter_spent_data))
        
        for data in filter_spent_data:
            self.filter_spent_table.setItem(row, 0, QTableWidgetItem(data['title']))
            self.filter_spent_table.setItem(row, 1, QTableWidgetItem(data['type']))
            self.filter_spent_table.setItem(row, 2, QTableWidgetItem(str(np.round(data['amount'],2))))
            self.filter_spent_table.setItem(row, 3, QTableWidgetItem(str(data['date'])))
            self.filter_spent_table.setItem(row, 4, QTableWidgetItem(data['category']))
            row += 1
    
    
    def filter_income_view(self):
        self.filter_income_page = self.stacked_widget.widget(4)
        self.stacked_widget.setCurrentWidget(self.filter_income_page)
        self.filter_income_table = self.findChild(QTableWidget, "filter_income_table")
        
        dataset = self.parent_api.login_user.get_all_bank_ent()
        type_filter = [element["type"] == "Rentrée" for element in dataset]
        
        filter_income_data = dataset[type_filter]
        row = 0
        self.filter_income_table.setRowCount(len(filter_income_data))
        
        for data in filter_income_data:
            self.filter_income_table.setItem(row, 0, QTableWidgetItem(data['title']))
            self.filter_income_table.setItem(row, 1, QTableWidgetItem(data['type']))
            self.filter_income_table.setItem(row, 2, QTableWidgetItem(str(np.round(data['amount'],2))))
            self.filter_income_table.setItem(row, 3, QTableWidgetItem(str(data['date'])))
            self.filter_income_table.setItem(row, 4, QTableWidgetItem(data['category']))
            row += 1
    
    
    def filter_category_view(self):
        self.filter_category_page = self.stacked_widget.widget(5) 
        self.stacked_widget.setCurrentWidget(self.filter_category_page)
        
        self.filter_category_table = self.findChild(QTableWidget, "filter_category_table")
        
        dataset = self.parent_api.login_user.get_all_bank_ent()
        category_filter = [element["category"] == self.category_filter_cbbox.currentText() for element in dataset]
        
        filter_category_data = dataset[category_filter]
        row = 0
        self.filter_category_table.setRowCount(len(filter_category_data))
        
        for data in filter_category_data:
            self.filter_category_table.setItem(row, 0, QTableWidgetItem(data['title']))
            self.filter_category_table.setItem(row, 1, QTableWidgetItem(data['type']))
            self.filter_category_table.setItem(row, 2, QTableWidgetItem(str(np.round(data['amount'],2))))
            self.filter_category_table.setItem(row, 3, QTableWidgetItem(str(data['date'])))
            self.filter_category_table.setItem(row, 4, QTableWidgetItem(data['category']))
            row += 1

        self.category_filter_cbbox.currentIndexChanged[str].connect(self.refresh_filter_category_view)
        
    
    def refresh_filter_category_view(self):
        dataset = self.parent_api.login_user.get_all_bank_ent()
        category_filter = [element["category"] == self.category_filter_cbbox.currentText() for element in dataset]
        
        filter_category_data = dataset[category_filter]
        row = 0
        self.filter_category_table.setRowCount(len(filter_category_data))
        
        for data in filter_category_data:
            self.filter_category_table.setItem(row, 0, QTableWidgetItem(data['title']))
            self.filter_category_table.setItem(row, 1, QTableWidgetItem(data['type']))
            self.filter_category_table.setItem(row, 2, QTableWidgetItem(str(np.round(data['amount'],2))))
            self.filter_category_table.setItem(row, 3, QTableWidgetItem(str(data['date'])))
            self.filter_category_table.setItem(row, 4, QTableWidgetItem(data['category']))
            row += 1
    
    def filter_month_view(self):
        self.filter_month_page = self.stacked_widget.widget(6)    
        self.stacked_widget.setCurrentWidget(self.filter_month_page)
        
        self.filter_month_table = self.findChild(QTableWidget, "filter_month_table")
        
        dataset = self.parent_api.login_user.get_all_bank_ent()
        
        month_year_filter = [str(pd.to_datetime(element["date"]).month_name()) + " - " + str(pd.to_datetime(element["date"]).year) == self.month_filter_cbbox.currentText() for element in dataset]
        filter_month_data = dataset[month_year_filter]
        row = 0
        self.filter_month_table.setRowCount(len(filter_month_data))
        
        for data in filter_month_data:
            self.filter_month_table.setItem(row, 0, QTableWidgetItem(data['title']))
            self.filter_month_table.setItem(row, 1, QTableWidgetItem(data['type']))
            self.filter_month_table.setItem(row, 2, QTableWidgetItem(str(np.round(data['amount'],2))))
            self.filter_month_table.setItem(row, 3, QTableWidgetItem(str(data['date'])))
            self.filter_month_table.setItem(row, 4, QTableWidgetItem(data['category']))
            row += 1
            
        self.month_filter_cbbox.currentIndexChanged[str].connect(self.refresh_filter_month_view)
        
        
    def refresh_filter_month_view(self):
        dataset = self.parent_api.login_user.get_all_bank_ent()
        
        month_year_filter = [str(pd.to_datetime(element["date"]).month_name()) + " - " + str(pd.to_datetime(element["date"]).year) == self.month_filter_cbbox.currentText() for element in dataset]
        filter_month_data = dataset[month_year_filter]
        row = 0
        self.filter_month_table.setRowCount(len(filter_month_data))
        
        for data in filter_month_data:
            self.filter_month_table.setItem(row, 0, QTableWidgetItem(data['title']))
            self.filter_month_table.setItem(row, 1, QTableWidgetItem(data['type']))
            self.filter_month_table.setItem(row, 2, QTableWidgetItem(str(np.round(data['amount'],2))))
            self.filter_month_table.setItem(row, 3, QTableWidgetItem(str(data['date'])))
            self.filter_month_table.setItem(row, 4, QTableWidgetItem(data['category']))
            row += 1
    
    
    def delete_form(self):
        delete_dialog = DeleteInputDialog(self.parent_api)
        if delete_dialog.exec_():
            msg = QMessageBox()
            msg.setWindowTitle("Entrée bien supprimée")
            msg.setText("L'entrée a bien été supprimée.")
            msg.exec_()
        
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Erreur")
            msg.setText("Désolé, une erreur est survenue")
            msg.exec_()
        
        
    
    def edit_form(self):
        edit_dialog = EditInputDialog(self.parent_api)
        if edit_dialog.exec_():
            msg = QMessageBox()
            msg.setWindowTitle("Entrée bien modifiée")
            msg.setText("L'entrée a bien été modifiée.")
            msg.exec_()
        
        else:
            msg = QMessageBox()
            msg.setWindowTitle("Erreur")
            msg.setText("Désolé, une erreur est survenue")
            msg.exec_()
        
if __name__ == "__main__":
    # Initialize app
    app = QApplication(sys.argv)
    account_man_window = accountManagerWindow()
    account_man_window.show()
    app.exec_()
