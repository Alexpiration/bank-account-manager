# -*- coding: utf-8 -*-
"""
Created on Thu Feb 17 23:01:29 2022

@author: alexpiration
"""

import time
import datetime

from .sqlite_ctxt import UserDBConn


class BankEntries:
    
    def __init__(self, title, entrie_type, amount, date, author_id, category="", description=""):
        self.title = title
        self.entrie_type = entrie_type
        self.amount = amount
        self.date = date
        self.category = category
        self.author_id = author_id
        self.description = description
        
        
    @property
    def b_id(self):
        sql_req = """select id from entries where title=:title and type=:type and amount=:amount and author_id=:author_id"""
        sql_dic = {'title': self.title, 'type': self.entrie_type, 'amount': self.amount, 'author_id': self.author_id}
        with UserDBConn(self.author_id) as u_conn:
            u_conn.execute(sql_req,sql_dic)
            res = u_conn.fetchone()[0]
        
        return res
      
    
    def add_entrie(self):
        now = time.strftime("%Y-%m-%d, %H:%M:%S")
        shape_entrie_type, shape_amount, shape_date, month = self.shape_data(self.entrie_type, self.amount, self.date)
        data = (self.title, shape_entrie_type, shape_amount, shape_date, self.description, self.category, month, now, self.author_id)
        
        with UserDBConn(self.author_id) as u_conn:
            sql_req = """insert into entries(title, type, amount, date, description, category, month, created_at, author_id)
                         values(?, ?, ?, ?, ?, ?, ?, ?, ?)"""
            u_conn.execute(sql_req, data)
        
        return True
        
    
    def shape_data(self, entrie_type, amount, date):
        shape_entrie_type = entrie_type.capitalize()
        shape_amount = amount.replace(",", ".")
        
        if shape_entrie_type == "Dépense":
            shape_amount = '-' + shape_amount
        
        datetime_obj = datetime.datetime.strptime(date, "%d/%m/%Y")
        shape_date = datetime_obj.strftime("%Y-%m-%d")
        month = datetime_obj.strftime("%B")
        
        return shape_entrie_type, shape_amount, shape_date, month
    
    
    @staticmethod
    def update_entrie(entrie_id, author_id, new_data: dict):
        if bool(new_data):
            sql_req = """update entries set """
            sql_data = tuple()
            
            for key in new_data:
                if len(new_data[key]):
                    sql_req += key + "=?, "
                    sql_data += (new_data[key],)
            
            sql_req += "where id=?"
            sql_req = sql_req.replace(", where", " where")
            sql_data += (entrie_id,)
            
            with UserDBConn(author_id) as u_conn:
                u_conn.execute(sql_req, sql_data)
        else:
            print("Aucune nouvelles données à mettre à jour.")
            
    
    @staticmethod
    def delete_entrie(entrie_id, author_id):
        sql_req = """delete from entries where id=:b_id"""
        
        with UserDBConn(author_id) as u_conn:
            u_conn.execute(sql_req, {'b_id': entrie_id})
        
        return True
            
            
    def __repr__(self):
        return f"{self.b_id}, {self.title}, {self.entrie_type}"