#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 16:45:18 2022

@author: alexpiration
"""

import os
import hashlib
import sqlite3
import time

import bcrypt
import numpy as np
from .sqlite_ctxt import UserDBConn, MainDBConn

class User:
    
    def __init__(self, username, password):
        self.username = username
        self.password = password
        
    
    # User method which define a user in the app
        
    @property
    def u_id(self):
        with MainDBConn() as main_c:
            main_c.execute("""select id from user where username=?""", (self.username,))
            res = main_c.fetchone()
            
        if res is None:
            print("Erreur dans le nom d'utilisateur")
        else:
            return res[0]
    
    def add_user(self):
        hash_user_pass = self.hash_passwd(self.password)
        now = time.strftime("%Y/%m/%d, %Hh%Mm%Ss")
        
        with MainDBConn() as main_c:    
            sql_request = """insert into user(username, password, created_at) values (?, ?, ?)"""
            main_c.execute(sql_request, (self.username, hash_user_pass, now))
        
        print("Utilisateur bien ajouté")
        
    
    def create_own_db(self):
        # # DEV MODE
        # dirname = os.path.dirname
        # db_folder = os.path.join(dirname(dirname(__file__)), 'db_dev_folder', 'user')
        
        # PROD MODE
        home_path = os.path.expanduser('~') 
        db_folder = os.path.join(home_path, 'bank_man', 'user')
        
        hash_name = hashlib.sha256(self.username.encode()).hexdigest()
        db_u_path = os.path.join(db_folder, 'user_' + hash_name + '.db')
        
        if not os.path.isfile(db_u_path):
            open(db_u_path, 'w+')
            
        u_db_conn = sqlite3.connect(db_u_path)
        u_db_c = u_db_conn.cursor()
        
        u_db_c.execute("""create table if not exists entries(
                        id integer primary key,
                        title varchar not null,
                        type varchar not null,
                        amount decimal not null,
                        date timestamp not null,
                        description text,
                        category varchar,
                        month timestamp not null,
                        created_at timestamp,
                        author_id integer)""")
        u_db_c.execute("""create table if not exists category(
                        id integer primary key,
                        name varchar not null,
                        created_at timestamp,
                        author_id integer)""")
        u_db_conn.commit()
        u_db_conn.close()
        
        print("Base pour utilisateur créée")
        
        now = time.strftime("%Y-%m-%d, %H:%M:%S")
        
        with MainDBConn() as main_c:                 
            main_c.execute("""insert into database_management(name, author_id, created_at) values(?, ?, ?)""", (str(hash_name), self.u_id, now))
        
        print("Base utilisateur bien consignée")
        
        
    def hash_passwd(self, user_pass):
        encode_pass = user_pass.encode('utf-8')
        hash_pass = bcrypt.hashpw(encode_pass, bcrypt.gensalt())
        
        return hash_pass.decode('utf-8')
        
    
    # User method interact with a user
    
    # User Authentication System
    
    def user_auth(self):
        if self.get_user():
            if bcrypt.checkpw(self.password.encode('utf-8'), self.get_passwd()):
                return True
        else:
            return False
    
    def get_user(self):
        with MainDBConn() as main_c:
            main_c.execute("""select username from user where id=?""", (self.u_id,))
            res = main_c.fetchone()
            return False if res is None else True
        
    
    def get_passwd(self):
        with MainDBConn() as main_c:
            main_c.execute("""select password from user where id=?""", (self.u_id,))
            res = main_c.fetchone()[0]
        
        return res.encode('utf-8')
    
    # End User Authentication System
    
    def get_user_db_name(self):
        with MainDBConn() as main_c:
            main_c.execute("""select name from database_management where author_id=?""", (self.id,))
            u_db_name = main_c.fetchone()[0]
        
        return u_db_name
    
    # Display Sold System
    
    def get_all_bank_ent(self):
        if self.user_auth():
            with UserDBConn(self.u_id) as u_conn:
                u_conn.execute("""select * from entries""")
                all_ent = np.array(u_conn.fetchall(), 
                                   dtype=[('id', 'i8'), ('title', 'U255'), ('type', 'U10'), ('amount', 'f4'), ('date', 'datetime64[D]'), ('description', 'U255'), ('category', 'U100'), ('month', 'U25'), ('created_at', 'U255'), ('author_id', 'i4')])
                
                return all_ent
        
        else:
            print("Accès refusé")
            
    
    def get_sold(self):
        entrie = self.get_all_bank_ent()
        sold = entrie['amount'].sum()
        return np.round(sold,2)
        
    
    # End Display Sold System
    
    def get_all_category(self):
        with UserDBConn(self.u_id) as u_conn:
            u_conn.execute("""select * from category""")
            res = u_conn.fetchall()
        
            return [r[1] for r in res]
    
        
    @staticmethod
    def get_bank_ent(bank_id, author_id):
        sql_req = """select * from entries where id=:bank_id"""
        with UserDBConn(author_id) as u_conn:
            u_conn.execute(sql_req, {"bank_id": bank_id})
            res = u_conn.fetchone()
        
        return res
    
    @staticmethod
    def add_category(category, author_id):
        now = time.strftime("%Y-%m-%d, %H:%M:%S")
        
        with UserDBConn(author_id) as u_conn:
            u_conn.execute("""select * from category where name=?""", (str(category),))
            check = u_conn.fetchone()
            
            if check == None:
                u_conn.execute("""insert into category(name, created_at, author_id) values(?, ?, ?)""", (str(category), now, author_id,))
                return True            
            else:
                
                return False

    