# -*- coding: utf-8 -*-
"""
Created on Sat Jan 29 14:44:17 2022

@author: Alexpiration
"""

from .sqlite_ctxt import MainDBConn

# SQLITE config

## Methode d'initialisation de l'application

def create_user_db():
    with MainDBConn() as main_c:
        main_c.execute("""create table if not exists user(
                        id integer primary key,
                        username varchar not null,
                        password text not null,
                        created_at timestamp not null)""")


def create_dbmanager_db():
    with  MainDBConn() as main_c:
        main_c.execute("""create table if not exists database_management (
                        id integer primary key,
                        name text not null,
                        author_id integer not null,
                        created_at TIMESTAMP not null,
                        foreign key (author_id) references user (id))""")

## Methode pour fonctionnalités de l'application
    