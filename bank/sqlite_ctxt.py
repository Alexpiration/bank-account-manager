#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  3 18:09:09 2022

@author: alexpiration
"""

import contextlib
import sqlite3
import os


@contextlib.contextmanager
def MainDBConn():
    # # DEV MODE
    # dirname = os.path.dirname
    
    # path = os.path.join(dirname(dirname(__file__)), 'db_dev_folder', 'main.db')
    # PROD MODE
    path = os.path.join(os.path.expanduser('~'), 'bank_man', 'main.db')
    
    try:
        conn = sqlite3.connect(path)
        cur = conn.cursor()
            
        # print("Connexion à la DB OK")
        
        yield cur
        
        conn.commit()
        conn.close()
        # print("Connexion bien fermée")
    
    except Exception as e:
        print(e)

@contextlib.contextmanager
def UserDBConn(author_id):
    # # DEV MODE
    # dirname = os.path.dirname
    # db_folder = os.path.join(dirname(dirname(__file__)), 'db_dev_folder', 'user')
    
    # PROD MODE
    db_folder = os.path.join(os.path.expanduser('~'), 'bank_man', 'user')
    u_db_name = get_user_db_name(author_id)
    u_path = os.path.join(db_folder, 'user_' + u_db_name + '.db')
    
    u_conn = sqlite3.connect(u_path)
    u_curs = u_conn.cursor()
    
    # print("Connexion à la DB OK")
    
    yield u_curs
    
    u_conn.commit()
    u_conn.close()
    # print("Connexion bien fermée")

def get_user_db_name(author_id):
    # # DEV MODE
    # dirname = os.path.dirname
    # path = os.path.join(dirname(dirname(__file__)), 'db_dev_folder', 'main.db')
    # PROD MODE
    path = os.path.join(os.path.expanduser('~'), 'bank_man', 'main.db')
    
    conn = sqlite3.connect(path)
    c = conn.cursor()
    
    c.execute("select name from database_management where author_id=?", (author_id,))
    res = c.fetchone()
    db_name = res[0]
    
    return db_name    