#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 16:44:57 2022

@author: alexpiration
"""

import os

from bank.db_init import create_dbmanager_db, create_user_db


def app_init():
    # # DEV MODE
    # db_folder = os.path.join(os.getcwd(), 'db_dev_folder')
    # PROD MODE
    home_path = os.path.expanduser('~') 
    db_folder = os.path.join(home_path, 'bank_man')
    
    # # DEV MODE
    # u_db_folder = os.path.join(os.getcwd(), 'db_dev_folder', 'user')
    
    # PROD MODE
    home_path = os.path.expanduser('~') 
    u_db_folder = os.path.join(home_path, 'bank_man', 'user')
    
    if not os.path.exists(db_folder):
        os.mkdir(db_folder)
        
    if not os.path.exists(u_db_folder):
        os.mkdir(u_db_folder)
        
    create_dbmanager_db()
    create_user_db()
        