#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 29 13:22:43 2022

@author: alexpiration
"""

from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton, QLineEdit, QCommandLinkButton
from PyQt5 import uic
import sys
import os

from bank.user import User
from gui.accountManagerWindow import accountManagerWindow
from gui.signinFormWindow import signinFormWindow


class loginWindow(QMainWindow):
    def __init__(self):
       super(loginWindow, self).__init__()
       
       # Load ui file of login Window
       uic.loadUi(os.path.join("gui", "ui", "loginWindow.ui"), self)
       
       # Get and Define Widgets presents in ui file
       self.app_name_label = self.findChild(QLabel, "appNameLabel")
       
       self.login_btn = self.findChild(QPushButton, "loginBtn")
       self.login_btn.clicked.connect(self.open_account_manager)
        
       self.username_form = self.findChild(QLineEdit, "usernameForm")
       self.password_form = self.findChild(QLineEdit, "passwordForm")
       self.password_form.setEchoMode(QLineEdit.Password)
       
       self.error_auth_label = self.findChild(QLabel, "errorAuthLabel")
       self.signin_link_btn = self.findChild(QCommandLinkButton, "signinCmdLinkBtn")
       self.signin_link_btn.clicked.connect(self.open_signin_form)
        
    def open_signin_form(self):
        self.signin_window = signinFormWindow()
        self.signin_window.show()
    
    def open_account_manager(self):
        username = self.username_form.text()
        pswd = self.password_form.text()
        
        user = User(username, pswd)
        
        if user.user_auth():
            self.login_user = user
            self.account_man_window = accountManagerWindow(self)
            
            # Display username in account name label
        
            account_name_label = self.account_man_window.account_name_label.text()
            self.account_man_window.account_name_label.setText(account_name_label.format(self.login_user.username))
            
            # Display sold in sold label
            self.account_man_window.display_user_sold()
            
            # Display account content in table widget
            self.account_man_window.load_data_table()
            self.account_man_window.show()
            
            self.hide()
            
        else:
            self.error_auth_label.setText("Cet utilisateur n'existe pas, ou le mot de passe entré est incorrect.")

        
        
if __name__ == "__main__":
    # Initialize app
    app = QApplication(sys.argv)
    login_window = loginWindow()
    login_window.show()
    app.exec_()
 