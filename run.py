#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 15:13:01 2022

@author: alexpiration
"""
from PyQt5.QtWidgets import QApplication
import sys

from bank import app_init
from gui.loginWindow import loginWindow


if __name__ == '__main__':
    app_init()
    
    app = QApplication(sys.argv)
    main = loginWindow()
    main.show()
    app.exec_()