#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 30 19:08:03 2022

@author: alexpiration
"""

from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton, QLineEdit, QMessageBox
from PyQt5 import uic
import sys
import os

from bank.user import User


class signinFormWindow(QMainWindow):
    def __init__(self):
        super(signinFormWindow, self).__init__()
        
        # Load ui file of login Window
        uic.loadUi(os.path.join("gui", "ui", "signinForm.ui"), self)
        
        # Get and Define Widgets presents in ui file
        self.username_field = self.findChild(QLineEdit, "userInputForm")
        
        self.pswd_field = self.findChild(QLineEdit, "pswdInputForm")
        self.pswd_field.setEchoMode(QLineEdit.Password)
        
        self.pswd_confirm_field = self.findChild(QLineEdit, "confInputPswdForm")
        self.pswd_confirm_field.setEchoMode(QLineEdit.Password)
        
        self.signin_error_auth_label = self.findChild(QLabel, "signinErrorAuthLabel")
        
        self.signin_submit_btn = self.findChild(QPushButton, "signinBtn")
        self.signin_submit_btn.clicked.connect(self.register_new_user)
        
        
    def register_new_user(self):
        print("Demarrage de la fonction")
        input_username = self.username_field.text().strip()
        input_pswd = self.pswd_field.text().strip()
        input_pswd_confirm = self.pswd_confirm_field.text().strip()
        
        if input_username and input_pswd and input_pswd_confirm:
            if input_pswd == input_pswd_confirm:
                new_user = User(input_username, input_pswd)
                new_user.add_user()
                new_user.create_own_db()
                
                msg = QMessageBox()
                msg.setWindowTitle("Utilisateur bien ajouté")
                msg.setText("Vous avez bien été ajouté, veuillez vous connecter.")
                msg.exec_()
                
                self.hide()
                
        else:
            self.signin_error_auth_label.setText("Veuillez remplir toutes les informations")
        
        
if __name__ == "__main__":
    # Initialize app
    app = QApplication(sys.argv)
    signin_form = signinFormWindow()
    signin_form.show()
    app.exec_()