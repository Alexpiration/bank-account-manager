# -*- coding: utf-8 -*-
"""
Created on Mon Apr 25 12:18:52 2022

@author: Alexpiration Boulot
"""

from PyQt5.QtWidgets import QApplication, QDialog, QDialogButtonBox, QLineEdit, QFormLayout, QComboBox

import sys
from bank.bank_entrie import BankEntries

class EditInputDialog(QDialog):
    def __init__(self, parent):
        super().__init__(parent)
        
        self.parent_api = parent
        
        self.setWindowTitle("Editer une entrée")
        
        self.entrie_list = QComboBox()
        self.entrie_list.addItems([f"{entrie['id']}, {entrie['title']}, {entrie['type']}" for entrie in self.parent_api.login_user.get_all_bank_ent()])
        self.entrie_selection = self.user_choice()
        
        self.entrie_title = QLineEdit(self)
        self.entrie_title.setText(str(self.entrie_selection[1]))
        self.entrie_type = QLineEdit(self)
        self.entrie_type.setText(str(self.entrie_selection[2]))
        self.entrie_amount = QLineEdit(self)
        self.entrie_amount.setText(str(self.entrie_selection[3]))
        self.entrie_date = QLineEdit(self)
        self.entrie_date.setText(str(self.entrie_selection[4]))
        self.entrie_category = QLineEdit(self)
        self.entrie_category.setText(str(self.entrie_selection[6]))
        self.entrie_description = QLineEdit(self)
        self.entrie_description.setText(str(self.entrie_selection[5]))
        
        self.btn_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, self)
        
        self.entrie_list.currentIndexChanged[str].connect(self.refresh_form)
        
        layout_form = QFormLayout(self)
        layout_form.addRow("Entrée :", self.entrie_list)
        layout_form.addRow("Titre :", self.entrie_title)
        layout_form.addRow("Type :", self.entrie_type)
        layout_form.addRow("Montant :", self.entrie_amount)
        layout_form.addRow("Date :", self.entrie_date)
        layout_form.addRow("Catégorie :", self.entrie_category)
        layout_form.addRow("Description :", self.entrie_description)
        layout_form.addWidget(self.btn_box)
        
        self.btn_box.accepted.connect(self.edit_and_accept)
        self.btn_box.rejected.connect(self.reject)
        
        
    def user_choice(self):
        entrie_user_choice = self.entrie_list.currentText()
        id_choice = int(entrie_user_choice.split(",", 1)[0])
        return self.parent_api.login_user.get_bank_ent(id_choice, self.parent_api.login_user.u_id)
    
    
    def refresh_form(self):
        new_entrie_choice = self.user_choice()
        self.entrie_title.setText(str(new_entrie_choice[1]))
        self.entrie_type.setText(str(new_entrie_choice[2]))
        self.entrie_amount.setText(str(new_entrie_choice[3]))
        self.entrie_date.setText(str(new_entrie_choice[4]))
        self.entrie_category.setText(str(new_entrie_choice[6]))
        self.entrie_description.setText(str(new_entrie_choice[5]))
        
    def edit_and_accept(self):
        new_data = {}
        new_data['title'] = self.entrie_title.text()
        new_data['type'] = self.entrie_type.text()
        new_data['amount'] = self.entrie_amount.text()
        new_data['date'] = self.entrie_date.text()
        new_data['category'] = self.entrie_category.text()
        new_data['description'] = self.entrie_description.text()
        
        BankEntries.update_entrie(self.user_choice()[0], self.parent_api.login_user.u_id, new_data)
        self.accept()


class DeleteInputDialog(QDialog):
    def __init__(self, parent):
        super().__init__(parent)
        
        self.parent_api = parent
        
        self.setWindowTitle("Supprimer une entrée")
        
        self.entrie_list = QComboBox()
        self.entrie_list.addItems([f"{entrie['id']}, {entrie['title']}, {entrie['type']}" for entrie in self.parent_api.login_user.get_all_bank_ent()])
        self.entrie_selection = self.user_choice()
        
        self.btn_box = QDialogButtonBox(QDialogButtonBox.Ok | QDialogButtonBox.Cancel, self)
        
        layout_form = QFormLayout(self)
        layout_form.addRow(self.entrie_list)
        layout_form.addWidget(self.btn_box)
        
        self.btn_box.accepted.connect(self.delete_and_accept)
        self.btn_box.rejected.connect(self.reject)
        
        
    def user_choice(self):
        entrie_user_choice = self.entrie_list.currentText()
        id_choice = int(entrie_user_choice.split(",", 1)[0])
        return self.parent_api.login_user.get_bank_ent(id_choice, self.parent_api.login_user.u_id)
        
    
    def delete_and_accept(self):
        BankEntries.delete_entrie(self.user_choice()[0], self.parent_api.login_user.u_id)
        self.accept()

if __name__ == "__main__":
    # Initialize app
    app = QApplication(sys.argv)
    custom_input_dialog = EditInputDialog()
    custom_input_dialog.show()
    app.exec_()